import { Component, OnInit } from '@angular/core';
import { server_url } from './consts/server.const';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'messenger_plus'

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.ws = new WebSocket(server_url);

    this.dataService.ws.onopen = () => {
      console.log('connection opened!');
    }

    this.dataService.ws.onclose = () => {
      this.dataService.ws = null;

      this.dataService.ws.onerror = (errorMsg: any) => alert(errorMsg);
    }
  }
}

