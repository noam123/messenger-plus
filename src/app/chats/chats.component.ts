import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { DataService } from '../data.service';
import { CLIENT_CODES } from '../enums/client-code.enum';
import { SERVER_CODES } from '../enums/server-code.enum';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit {

  currDstID = -1;
  currTextarea = "";
  currDstUser = "";

  sendReq: any;
  username = "";
  users: any;

  constructor(private dataService: DataService) {
    this.username = dataService.username;
    this.users = dataService.users;
  }

  ngOnInit(): void {
    this.dataService.ws.onmessage = (serverMsg: any) => {
      this.handleServerResponse(JSON.parse(serverMsg.data));
    }
  }

  handleServerResponse(msg: any): void {
    
    switch (msg.type) {
      case SERVER_CODES.UPDATE_CHATS_LIST:
        this.dataService.connectedUsers = msg.connectedUsers;
        this.dataService.users.length = 0;

        this.dataService.connectedUsers.forEach(user => this.dataService.users.push(user.name))
        this.dataService.users = this.dataService.users;

        // Updates chatsArr as well
        this.dataService.connectedUsers.forEach(user => {
          if (this.dataService.chatsArr.findIndex(chat => chat.destinationID == user.ID) == -1){
            this.dataService.chatsArr.push({ destinationID: user.ID, textarea: "" });
           }
        })
        break;
      case SERVER_CODES.GET_PRIVATE_MESSAGE:

        // Adds the message to the compatible textarea
        this.dataService.chatsArr[this.dataService.chatsArr.findIndex(chat => chat.destinationID == msg.sourceID)].textarea
          += this.dataService.connectedUsers[this.dataService.connectedUsers.findIndex(user => user.ID == msg.sourceID)].name
          + ': ' + msg.message + '\n';

        if (this.currDstID == msg.sourceID) {
          this.currTextarea = this.dataService.chatsArr[this.dataService.chatsArr.findIndex(chat => chat.destinationID == msg.sourceID)].textarea;
        }
        break;
    }
  }

  submitMsg(): void {

    if (!this.isReadyToSendMessage()) {
      return;
    }

    var msgToSend = (<HTMLInputElement>document.getElementById('msgToSend')).value;
    
    var body = {
      type: CLIENT_CODES.SEND_PRIVATE_MESSEGE, sourceID: this.dataService.userID
      , destinationID: this.currDstID, message: msgToSend
    };

    this.dataService.ws.send(JSON.stringify(body));

    this.dataService.chatsArr[this.dataService.chatsArr.findIndex(chat => chat.destinationID == this.currDstID)].textarea += 'You: ' + msgToSend + '\n';
    this.currTextarea = this.dataService.chatsArr[this.dataService.chatsArr.findIndex(chat => chat.destinationID == this.currDstID)].textarea;

    // Resets chat line
    (<HTMLInputElement>document.getElementById('msgToSend')).value = "";
  }

  isReadyToSendMessage(): boolean {
    // Checks if the user entered a name
    if (this.dataService.username == "Not connected") {
      alert("Login is required first");
      return false;
    }

    if (this.currDstID == -1) {
      alert('Choose someone to send a message to');
      return false;
    }

    // Checks if user is connected to server
    if (!this.dataService.ws) {
      alert('Server is probably off');
      return false;
    }

    return true;
}

  clearChat(): void {
    this.dataService.chatsArr[this.dataService.chatsArr.findIndex(chat => chat.destinationID == this.currDstID)].textarea = "";
    this.currTextarea = "";
  }

  clickedUsername(name: string) {
    this.currDstUser = name;
    this.currDstID = this.dataService.connectedUsers[this.dataService.connectedUsers.findIndex(user => user.name == name)].ID;
    
    this.currTextarea = this.dataService.chatsArr[this.dataService.chatsArr.findIndex(chat => chat.destinationID == this.currDstID)].textarea;
  }
}
