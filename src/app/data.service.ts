import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  ws: any;
  username = "Not connected";
  userID = -1;
  users: any[] = [];
  connectedUsers: any[] = [];
  chatsArr: any[] = [];

  constructor() { }
}
