import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { CLIENT_CODES } from '../enums/client-code.enum';
import { SERVER_CODES } from '../enums/server-code.enum';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router) {}

  ngOnInit(): void {
    this.dataService.ws.onmessage = (serverMsg: any) => {
      var msg = JSON.parse(serverMsg.data);
      console.log(msg);
      switch (msg.type) {
        case SERVER_CODES.LOGGED_IN_SUCCESSFULY:
          this.dataService.userID = msg.newID;
          this.dataService.username = msg.username;
          this.router.navigate(['/']);
          break;
        case SERVER_CODES.FAILED:
          alert('Username or Password are not correct');
          break;
      }
    }
  }

  loginBtnClicked(): void {
    var username = (<HTMLInputElement>document.getElementById('username')).value;
    var password = (<HTMLInputElement>document.getElementById('password')).value;

    if (username.length == 0 || password.length == 0) {
      alert('username and password must contain characters');
    }

    var body = { type: CLIENT_CODES.LOGIN, username: username, password: password };
    this.dataService.ws.send(JSON.stringify(body));
  }


}
