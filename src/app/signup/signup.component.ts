import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { DataService } from '../data.service';
import { CLIENT_CODES } from '../enums/client-code.enum';
import { SERVER_CODES } from '../enums/server-code.enum';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit{

  constructor(private dataSerive: DataService, private router: Router) { }

  ngOnInit(): void {
    this.dataSerive.ws.onmessage = (serverMsg: any) => {
      var msg = JSON.parse(serverMsg.data);
      console.log(msg);
      switch (msg.type) {
        case SERVER_CODES.SIGNED_UP_SUCCESSFULY:
          this.router.navigate(['/login']);
          break;
        case SERVER_CODES.FAILED:
          alert('Username or Password are not correct');
          break;
      }
    }
  }

  signupBtnClicked(): void {
    var username = (<HTMLInputElement>document.getElementById('username')).value;
    var password = (<HTMLInputElement>document.getElementById('password')).value;

    if (username.length == 0 || password.length == 0) {
      alert('username and password must contain characters');
    }
    
    var body = { type: CLIENT_CODES.SIGNUP, username: username, password: password };
    this.dataSerive.ws.send(JSON.stringify(body));
  }
}
