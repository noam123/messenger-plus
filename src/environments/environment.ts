// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBOytKDs9y5kuRQg1z2kCgHvmJ7gL4wb0U",
    authDomain: "messenger-plus-ni.firebaseapp.com",
    projectId: "messenger-plus-ni",
    storageBucket: "messenger-plus-ni.appspot.com",
    messagingSenderId: "90581820997",
    appId: "1:90581820997:web:ac0c78508a5a5eec46a52c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
